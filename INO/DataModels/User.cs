﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace INO.DataModels
{
    public class User
    {
        /// <summary>
        /// unique user's ID number
        /// </summary>
        public int ID { get; set; }
        public DateTime AccountCreated { get; set; }
        public DateTime AccountDeleted { get; set; }
        public List<Announcement> Observed { get; set; }
        /// <summary>
        /// list of currently valid user's announcements
        /// </summary>
        public List<Announcement> CurrentAnnoucements { get; set; }
        /// <summary>
        /// list of terminated user's announcements
        /// </summary>
        public List<Announcement> TerminatedAnnouncements { get; set; }
        public List<Conversation> Conversations { get; set; }
        public bool IsDeleted { get; set; }
        public InvoiceData DataForInvoice { get; set; }

    }
}
