﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INO.DataModels
{
    public class Category
    {
        public int Id { get; set; }
        public String Name { get; set; }
        /// <summary>
        /// subcategories IDs
        /// </summary>
        public List<int> Subcategories { get; set; }
        public decimal HighlightPrice { get; set; }
        /// <summary>
        /// returns number of announcements in category
        /// </summary>
        public int AnnouncementsNumber { get; private set; }
    }
}
