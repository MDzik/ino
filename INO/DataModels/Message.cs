﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INO.DataModels
{
    public class Message
    {
        public String Content { get; set; }
        public DateTime Sended { get; set; }
        public DateTime Viewed { get; set; }
    }
}
