﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INO.DataModels
{
    public class InvoiceData
    {
        public String CompanyName { get; set; }
        public String Street { get; set; }
        public String City { get; set; }
        public decimal PostalCode { get; set; }
        public decimal NIP { get; set; }
            
    }
}
