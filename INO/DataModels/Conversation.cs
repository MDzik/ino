﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INO.DataModels
{
    public class Conversation
    {
        public Tuple<User,User> Interlocutors { get; set; }
        public List<Message> Messages { get; set; }
    }
}
