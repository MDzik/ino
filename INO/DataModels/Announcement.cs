﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INO.DataModels
{
    public class Announcement
    {
        public string Title { get; set; }
        /// <summary>
        /// stores description of an announcement
        /// </summary>
        public String Content  { get; set; }
        /// <summary>
        /// Announcement unique ID number
        /// </summary>
        public int ID { get; set; }
        public Decimal Price { get; set; }
        public String Location { get; set; }
        /// <summary>
        /// user 'current' name - every user may create announcements under different names
        /// </summary>
        public String AdvertiserName { get; set; }
        public User Advertiser { get; set; }
        public DateTime Started { get; set; }
        public DateTime Expires { get; set; }
        public DateTime [] Prolongations { get; set; }
        public DateTime[] Changes { get; set; }
        /// <summary>
        /// phone number
        /// </summary>
        public String Phone { get; set; }
        /// <summary>
        /// advertiser's Skype
        /// </summary>
        public String Skype { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsHighlighted { get; set; }
        /// <summary>
        /// number of times the annoucement was viewed
        /// </summary>
        public int Views { get; set; }
        /// <summary>
        /// number of times the advertiser's phone number was viewed
        /// </summary>
        public int PhoneViews { get; set; }
        /// <summary>
        /// number of times the advertiser's Skype was viewed
        /// </summary>
        public int SkypeViews { get; set; }
        /// <summary>
        /// List of users following announcement
        /// </summary>
        public List<User> Followers { get; set; }
        public int FollowersNumber
        {
            get { return Followers.Count; }
        }
        public bool ruleViolation { get; set; }
        /// <summary>
        /// contains list of Photo links
        /// </summary>
        public List<String> Photos { get; set; }
        /// <summary>
        /// stores link to main Photo
        /// </summary>
        public String MainPhoto { get; set; }
    }
}
