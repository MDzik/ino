﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using INO.DataModels;

namespace ConversationExport
{
    public class ConversationShare
    {
        /// <summary>
        /// Allows to share serialized list of Conversation object
        /// </summary>
        /// <param name="conversations">List of Conversation to be serialized</param>
        /// <returns> Sended objects, serialized to Json </returns>
        public string ConversationToJSON(List<Conversation> conversations)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Allows to share serialized list of Conversation object
        /// </summary>
        /// <param name="conversations">List of Conversation to be serialized</param>
        /// <returns> Sended objects, serialized to XML </returns>
        public string ConversationToXML(List<Conversation> conversations)
        {
            throw new NotImplementedException();
        }
    }
}
