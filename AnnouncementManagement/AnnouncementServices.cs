﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using INO.DataModels;

namespace AnnouncementManagement
{
    public class AnnouncementServices
    {

        public Announcement AddAnnouncement(Announcement toAdd)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Refreshes announcement - makes it valid longer
        /// </summary>
        /// <param name="toRefresh">Announcement to be refreshed</param>0
        /// <returns></returns>
        public INO.DataModels.Announcement RefreshAnnouncement(INO.DataModels.Announcement toRefresh)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Allows editing annoucement
        /// </summary>
        /// <param name="toEdit">Announcement to be refreshed</param>
        /// <param name="property">Property to be updated</param>
        /// <param name="newValue">New value of updating value</param>
        /// <returns>Updeted announcement</returns>
        public INO.DataModels.Announcement EditAnnouncement(INO.DataModels.Announcement toEdit, string property, string newValue)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Allows deleting announcement
        /// </summary>
        /// <param name="toDelete">Announcement to be deleted</param>
        public void DeleteAnnouncement(INO.DataModels.Announcement toDelete)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Reports announcement to administrator.
        /// </summary>
        /// <param name="toRefresh">Announcement to be reported</param>
        /// <returns>Reported announcement</returns>
        public INO.DataModels.Announcement ReportAnnouncement(INO.DataModels.Announcement toRefresh)
        {
            throw new NotImplementedException();
        }

    }
}
