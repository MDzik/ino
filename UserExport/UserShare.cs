﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using INO.DataModels;

namespace UserExport
{
    public class UserShare
    {
        /// <summary>
        /// Allows to share serialized list of User object
        /// </summary>
        /// <param name="user">List of Users to be serialized</param>
        /// <returns> Sended objects, serialized to Json </returns>
        public string UserToJSON(List<User> user)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Allows to share serialized list of User object
        /// </summary>
        /// <param name="user">List of Users to be serialized</param>
        /// <returns> Sended objects, serialized to XML </returns>
        public string UserToXML(List<User> user)
        {
            throw new NotImplementedException();
        }
    }
}
