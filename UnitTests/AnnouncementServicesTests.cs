﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AnnouncementManagement;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using INO.DataModels;
namespace AnnouncementManagement.Tests
{
    [TestClass()]
    public class AnnouncementServicesTests
    {
        [TestMethod()]
        public void RefreshAnnouncementTest()
        {
            throw new NotImplementedException();
        }

        [TestMethod()]
        public void EditAnnouncementTest()
        {
            Announcement toChange = new Announcement();
            AnnouncementServices annServices = new AnnouncementServices();
            annServices.AddAnnouncement(toChange);

            string newTitle =  toChange.Title == "Absolutely new title" ? newTitle = "new new title" : newTitle = "Absolutely new title" ;
  
            annServices.EditAnnouncement(toChange, "Title", newTitle);

            string received = new AnnouncementDatabaseManager.AnnouncementDatabaseServices().GetAnnouncement(toChange.ID).Title;

            Assert.AreEqual(newTitle, received);

        }

        [TestMethod()]
        public void DeleteAnnouncementTest()
        {
            throw new NotImplementedException();
        }

        [TestMethod()]
        public void ReportAnnouncementTest()
        {
            throw new NotImplementedException();
        }
    }
}
