﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagement;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using INO.DataModels;
namespace UserManagement.Tests
{
    [TestClass()]
    public class UserServicesTests
    {
        [TestMethod()]
        public void AddUserTest()
        {
            User toAdd = new User();
            new UserServices().AddUser(toAdd);
            var id = toAdd.ID;

            User received = new UserDatabaseManager.UserDatabaseServices().GetUser(id);

            Assert.AreEqual(toAdd, received);
        }

        [TestMethod()]
        public void DeleteUserTest()
        {
            throw new NotImplementedException();
        }

        [TestMethod()]
        public void ChangeUserDataTest()
        {
            throw new NotImplementedException();
        }
    }
}
