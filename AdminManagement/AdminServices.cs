﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using INO.DataModels;

namespace AdminManagement
{
    public class AdminServices
    {
        /// <summary>
        /// Alows to Send invoice to specified user
        /// </summary>
        /// <param name="user">User, to whom invoice wil be sended </param>
        /// <param name="month">Invoiced month</param>
        /// <returns></returns>
        public void toInvoice(User user, DateTime month)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Allows to ban user
        /// </summary>
        /// <param name="toBan">User to be banned</param>
        /// <returns>Banned user</returns>
        public User BanUser(User toBan)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Allows to moderate announcement
        /// </summary>
        /// <param name="toModerate">Announcement to be moderate</param>
        /// <returns>Moderated announcement</returns>
        public Announcement ModerateAnnouncement(Announcement toModerate)
        {
            throw new NotImplementedException();
        }

    }
}
