﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using INO.DataModels;

namespace UserManagement
{
    public class UserServices
    {
        /// <summary>
        /// Creates new user
        /// </summary>
        /// <returns>User with given ID</returns>
        public User AddUser(User user)
        {
            throw new NotImplementedException();
        }
        
        /// <summary>
        /// Deletes an user
        /// </summary>
        /// <param name="toDelete">User to be deleted</param>
        public void DeleteUser(INO.DataModels.User toDelete)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Changes user's data
        /// </summary>
        /// <param name="toChange">User to be changed</param>
        /// <param name="parameter">name of parameter to be changed</param>
        /// <param name="Data">new value of updated parameter</param>
        /// <returns>Updated user</returns>
        public User ChangeUserData(User toChange, String parameter, String Data)
        {
            throw new NotImplementedException();
        }
                
    }
}
