﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using INO.DataModels;

namespace AnnouncementExport
{
    public class AnnouncementShare
    {
        /// <summary>
        /// Allows to share serialized list of Announcement object
        /// </summary>
        /// <param name="announcement">List of Announcements to be serialized</param>
        /// <returns> Sended objects, serialized to Json </returns>
        public string AnnouncementToJSON(List<Announcement> announcements)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Allows to share serialized list of Announcement object
        /// </summary>
        /// <param name="announcement">List of Announcements to be serialized</param>
        /// <returns> Sended objects, serialized to XML </returns>
        public string AnnouncementToXML(List<Announcement> announcements)
        {
            throw new NotImplementedException();
        }
    }
}
