﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using INO.DataModels;

namespace VisualizationManagement
{
    public class VisualizationServices
    {
        /// <summary>
        /// Get's a given number of announcements
        /// from specified category
        /// to display on webpage
        /// </summary>
        /// <param name="category">Category of announcements</param>
        /// <param name="quantity">Quantity of received announcements</param>
        /// <returns>List of first announcements</returns>
        public List<Announcement> getFirstAnnouncements(Category category, int quantity)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get's a given number of messages from specified conversation
        /// to display on webpage
        /// </summary>
        /// <param name="conversation">Conversation, form which messages ate taken</param>
        /// <param name="quantity">Quantity of returned messages</param>
        /// <returns>List of first messages </returns>
        public List<Message> getFirstMessages(Conversation conversation, int quantity)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Returns first finished announcements of an user,
        /// sorted from earliest to latest
        /// to display on webpage
        /// </summary>
        /// <param name="user">user, which announcements will be returned</param>
        /// <param name="quantity">Quantity of returned announcemets</param>
        /// <returns>List of returned announcements</returns>
        public List<Announcement> getFinishedAnnouncements(User user, int quantity)
        {
            throw new NotImplementedException();
        }




    }
}
